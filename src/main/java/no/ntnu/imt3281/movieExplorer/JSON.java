package no.ntnu.imt3281.movieExplorer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Used to parse JSON data and provide easy access to the content of the data contained in the JSON string.
 * 
 * @author oeivindk
 *
 */
public class JSON {
	HashMap<String, Object>	items = new HashMap<String, Object>();
	ArrayList<Object> arrayItems = new ArrayList<Object>();
	
	/**
	 * Creates a new JSON object, takes a string with JSON formated data as input.
	 * 
	 * @param jsoninput the string containing the JSON data to parse.
	 */
	public JSON(String jsoninput) {
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(jsoninput);
			extractFromJSON(obj);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the JSON object with the given name/key.
	 * 
	 * @param key the key to retrieve the JSON object for.
	 * @return a JSON object with all data for this key.
	 */
	public JSON get (String key) {
		return (JSON) items.get(key);
	}
	
	/**
	 * Get the JSON object for the given index. The current json objec is in fact an array, fetch
	 * the JSON object at the given index in this array.
	 * 
	 * @param idx the index to fetch the JSON object from.
	 * @return a JSON object with all data from this index.
	 */
	public JSON get (int idx) {
		return (JSON) arrayItems.get(idx);
	}
	
	/**
	 * This JSON object has a discreet value for this key, return this value.
	 * 
	 * @param key the key to retrieve the value for.
	 * @return the value for this key
	 */
	public Object getValue (String key) {
		return items.get(key);
	}
	
	/**
	 * Get the value for the given index. The current JSON object is in fact an array, fetch
	 * the value at the given index in this array.
	 * 
	 * @param idx the index to fetch the value from.
	 * @return the value for the given index.
	 */
	public Object getValue (int idx) {
		return arrayItems.get(idx);
	}
	
	/**
	 * Get the size (number of elements at root level) of this JSON object.
	 * 
	 * @return the size of this JSON object.
	 */
	public int size() {
		if (items.size()>0) {
			return items.size();
		} else {
			return arrayItems.size();
		}
	}
	
	/**
	 * @return content of JSON object as a string.
	 */
	public String toString() {
		if (items.size()>0) {
			return items.toString();
		} else {
			return arrayItems.toString();
		}
	}
	
	private JSON(JSONObject item) {
		extractFromJSON(item);
	}

	private JSON(JSONArray item) {
		extractFromJSON(item);
	}

	private void extractFromJSON(Object obj) {
		if (obj instanceof JSONObject) {
			JSONObject json = (JSONObject)obj;
			Iterator<String> keys = json.keySet().iterator();
			while (keys.hasNext()) {
				String key = keys.next();
				Object item = json.get(key);
				if (item instanceof JSONObject) {
					items.put(key, new JSON((JSONObject)item));
				} else if (item instanceof JSONArray) {
					items.put(key,  new JSON((JSONArray)item));
				} else {
					items.put(key, item);
				}
			}
		} else if (obj instanceof JSONArray) {
			JSONArray json = (JSONArray)obj;
			for (int i=0; i<json.size(); i++) {
				Object item = json.get(i);
				if (item instanceof JSONObject) {
					arrayItems.add(new JSON((JSONObject)item));
				} else if (item instanceof JSONArray) {
					arrayItems.add(new JSON((JSONArray)item));
				} else {
					arrayItems.add(item);
				}
			}
		}
	}
}
