package no.ntnu.imt3281.movieExplorer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * Controller for the about dialog. Contains functionality for clearing the disc cache and database cache as
 * well as for finding the size of the database and cache folders.
 * 
 * @author oeivindk
 *
 */
public class AboutDialog {
    @FXML private Label dbBuffer;
    @FXML private Label poster;
    @FXML private Label profile;
    @FXML private Label backdrop;
    @FXML private Label logo;
    @FXML private Label still;

    @FXML
    /**
     * Called when the "Clear cache" button is pressed, removes all images from the cache.
     * 
     * @param event not used
     */
    void clearCache(ActionEvent event) {
    		MovieExplorer.config.clearCache();
    		init();	// Update the GUI of the dialog
    }

    @FXML
    /**
     * Called when the "Empty DB" button is pressed, removes all json data from database.
     * 
     * @param event not used
     */
    void emptyDB(ActionEvent event) {
    		try {
			Statement stmnt = MovieExplorer.con.createStatement();
			String sql = "DELETE FROM buffer";
			stmnt.execute(sql);
			init();	// Update the GUI of the dialog
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Get the size of the datqbase and all cache folders, updates the labels to show correct data to the user.
     */
	public void init() {
		String sql = "select tableName, (select sum(numallocatedpages * pagesize) from new org.apache.derby.diag.SpaceTable('APP', t.tablename) x) as size from SYS.SYSTABLES t order by size desc";
		try {
			Statement stmnt = MovieExplorer.con.createStatement();
			ResultSet res = stmnt.executeQuery(sql);
			int sum = 0;
			while (res.next()) {
				sum += res.getInt("size");
			}
			dbBuffer.setText(sum/1024+" KB");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		poster.setText(MovieExplorer.config.getPosterCacheSize()/1024+" KB");
		profile.setText(MovieExplorer.config.getProfileCacheSize()/1024+" KB");
		backdrop.setText(MovieExplorer.config.getBackdropCacheSize()/1024+" KB");
		logo.setText(MovieExplorer.config.getLogoCacheSize()/1024+" KB");
		still.setText(MovieExplorer.config.getStillCacheSize()/1024+" KB");
	}
}
