package no.ntnu.imt3281.movieExplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MovieExplorer extends Application {
	public static final String API_KEY = "a47f70eb03a70790f5dd711f4caea42d";
	public static final String dbURL = "jdbc:derby:movieExplorerDB"; 
	public static final TheMovieDBConfiguration config = new TheMovieDBConfiguration();
	public static Connection con;
	public static Stage stage;

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Movie explorer");
		BorderPane gui = (BorderPane)FXMLLoader.load(getClass().getResource("GUI.fxml"));
		Scene myScene = new Scene(gui);
		primaryStage.setScene(myScene);
		primaryStage.show();
		stage = primaryStage;
	}
	
	@Override
	public void stop() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			con = DriverManager.getConnection(dbURL);
		} catch (SQLException e) {	// Probably because the database doesn't exist
			try {
				con = DriverManager.getConnection(dbURL+";create=true");
				MovieExplorer.createTables(con);
			} catch (SQLException sqle) {
				System.out.println("Unable to create database");
				System.exit(0);
			}
		}
		Preferences prefs = Preferences.userRoot().node(Class.class.getPackage().getName());
		String tmpFolder = prefs.get("tmpFolder", "");
		if (!tmpFolder.equals("")) {				// If tmp folder exists, use it.
			config.setTmpFolder(tmpFolder);
		}
		launch(args);
	}

	private static void createTables(Connection con) {
		String sql = "CREATE TABLE genres (id bigint NOT NULL, name varchar(255) NOT NULL, PRIMARY KEY (id))";
		String sql1 = "CREATE TABLE buffer (id bigint NOT NULL, mediaType char(16) NOT NULL, json clob, PRIMARY KEY (id, mediaType))";
		try {
			Statement stmnt = con.createStatement();
			stmnt.execute(sql);
			stmnt.execute(sql1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
