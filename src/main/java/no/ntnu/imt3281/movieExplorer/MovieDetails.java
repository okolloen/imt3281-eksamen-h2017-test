package no.ntnu.imt3281.movieExplorer;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MovieDetails {
	@FXML private Label title;
    @FXML private ImageView poster;
    @FXML private TextArea overview;
    @FXML private Label genres;

    public void init(long id) {
    		JSON data = Search.movie(id);
    		title.setText((String) data.getValue("title"));
    		overview.setText((String) data.getValue("overview"));
    		poster.setImage(new Image(MovieExplorer.config.getPosterURL((String) data.getValue("poster_path"))));
    		String tmp = "";
    		for (int i=0; i<data.get("genres").size(); i++) {
    			tmp += data.get("genres").get(i).getValue("name")+"\n";
    		}
    		genres.setText(tmp);
    }
    
}