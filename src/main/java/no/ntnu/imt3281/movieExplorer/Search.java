package no.ntnu.imt3281.movieExplorer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Class contains static functions that is used to query theMovieDB for information about
 * movies/people/tv-series.
 * 
 * @author oeivindk
 *
 */
public class Search {

	/**
	 * Search for people/movies/tv-series containing the word(s) in query.
	 * 
	 * @param query what to search for
	 * @return a JSON  object containing the response from the query
	 */
	public static JSON multiSearch(String query) {
		try {
			String res = Unirest.get("https://api.themoviedb.org/3/search/multi?language=en-US&page=1&include_adult=false")
					.queryString("query", query)
					.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
			return new JSON(res);
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Search for people taking part in the movie/tv-series with the given id.
	 * 
	 * @param id the id of the movie/tv-series to be queried
	 * @return a JSON object containing the response from the query
	 */
	public static JSON actors(long id) {
		JSON json = null;
		try {
			Statement stmnt = MovieExplorer.con.createStatement();
			ResultSet res = stmnt.executeQuery("SELECT json FROM buffer WHERE mediaType='movieCredits' AND id="+id);
			if (res.next()) {
				json = new JSON(res.getString("json"));
			} else {
				String response = Unirest.get("https://api.themoviedb.org/3/movie/"+id+"/credits")
					.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
				PreparedStatement ps = MovieExplorer.con.prepareStatement("INSERT INTO buffer (id, mediaType, json) VALUES (?, ?, ?)");
				ps.setInt(1,  (int) id);
				ps.setString(2, "movieCredits");
				ps.setString(3, response);
				ps.execute();
				json = new JSON(response);
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	/**
	 * Search for movies/tv-series that the person with the given id takes part in.
	 * 
	 * @param id the id of the person we want to find movies/tv-series for
	 * @return a JSON object containing the response from the query
	 */
	public static JSON takesPartIn(long id) {
		JSON json = null;
		try {
			Statement stmnt = MovieExplorer.con.createStatement();
			ResultSet res = stmnt.executeQuery("SELECT json FROM buffer WHERE mediaType='people' AND id="+id);
			if (res.next()) {
				json = new JSON(res.getString("json"));
			} else {
				String response = Unirest.get("https://api.themoviedb.org/3/discover/movie?language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1")
						.queryString("api_key", MovieExplorer.API_KEY)
						.queryString("with_people", id).asString().getBody();
				PreparedStatement ps = MovieExplorer.con.prepareStatement("INSERT INTO buffer (id, mediaType, json) VALUES (?, ?, ?)");
				ps.setInt(1,  (int) id);
				ps.setString(2, "people");
				ps.setString(3, response);
				ps.execute();
				json = new JSON(response);
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * Find information about a movie given its ID.
	 * 
	 * @param id the theMovieDB id of the movie to find information about.
	 * @return a JSON object with information about the requested movie.
	 */
	public static JSON movie(long id) {
		JSON json = null;
		try {
			Statement stmnt = MovieExplorer.con.createStatement();
			ResultSet res = stmnt.executeQuery("SELECT json FROM buffer WHERE mediaType='movie' AND id="+id);
			if (res.next()) {
				json = new JSON(res.getString("json"));
			} else {
				String response = Unirest.get("https://api.themoviedb.org/3/movie/"+id+"?api_key=&language=en-US")
					.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
				PreparedStatement ps = MovieExplorer.con.prepareStatement("INSERT INTO buffer (id, mediaType, json) VALUES (?, ?, ?)");
				ps.setInt(1,  (int) id);
				ps.setString(2, "movie");
				ps.setString(3, response);
				ps.execute();
				json = new JSON(response);
			}
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
}
