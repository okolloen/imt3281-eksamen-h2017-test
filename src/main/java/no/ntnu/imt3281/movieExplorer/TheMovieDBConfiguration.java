package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicLong;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Class is used to parse and store configuration information for theMovieDB.
 * Stores information about where to get images (backdrop, logo, poster, profile and still images
 * from movies.
 * @author oeivindk
 *
 */
public class TheMovieDBConfiguration {
	private String backdrop;
	private String logo;
	private String poster;
	private String profile;
	private String still;
	private String baseURL;
	private String tmpFolder = "";
	
	/**
	 * Initialize all values from given json string. This should be read elsewhere.
	 * 
	 * @param jsonInput string containing information from https://api.themoviedb.org/3/configuration?api_key=.....
	 */
	public TheMovieDBConfiguration(String jsonInput) {
		JSON json = new JSON(jsonInput);
		initFromJSON(json);
	}

	/**
	 * @param json
	 */
	private void initFromJSON(JSON json) {
		baseURL = (String) json.get("images").getValue("base_url");
		backdrop = (String) json.get("images").get("backdrop_sizes").getValue(json.get("images").get("backdrop_sizes").size()-2);
		logo = (String) json.get("images").get("logo_sizes").getValue(json.get("images").get("logo_sizes").size()-2);
		poster = (String) json.get("images").get("poster_sizes").getValue(json.get("images").get("poster_sizes").size()-2);
		profile = (String) json.get("images").get("profile_sizes").getValue(json.get("images").get("profile_sizes").size()-2);
		still = (String) json.get("images").get("still_sizes").getValue(json.get("images").get("still_sizes").size()-2);
	}

	public TheMovieDBConfiguration() {
		try {
			String res = Unirest.get("https://api.themoviedb.org/3/configuration")
					.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
			initFromJSON(new JSON(res));
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get url for backdrop image with given id
	 * 
	 * @param id the id to get url for
	 * @return complete url used to retrieve backdrop image.
	 */
	public String getBackdropURL(String id) {
		return downloadIfNeeded(id, backdrop);		
	}

	/**
	 * Get the size of the files in the backdrop cache folder.
	 * 
	 * @return the size of the files in the backdrop cache folder.
	 */
	public long getBackdropCacheSize() {
		return size(new File(tmpFolder+"/"+backdrop).toPath());
	}

	/**
	 * Get url for logo image with given id
	 * 
	 * @param id the id to get url for
	 * @return complete url used to retrieve logo image.
	 */
	public String getLogoURL(String id) {
		return downloadIfNeeded(id, logo);
	}

	/**
	 * Get the size of the files in the logo cache folder.
	 * 
	 * @return the size of the files in the logo cache folder.
	 */
	public long getLogoCacheSize() {
		return size(new File(tmpFolder+"/"+logo).toPath());
	}
	
	
	/**
	 * Get url for poster image with given id
	 * 
	 * @param id the id to get url for
	 * @return complete url used to retrieve poster image.
	 */
	public String getPosterURL(String id) {
		return downloadIfNeeded(id, poster);
	}
	
	/**
	 * Get the size of the files in the poster cache folder.
	 * 
	 * @return the size of the files in the poster cache folder.
	 */
	public long getPosterCacheSize() {
		return size(new File(tmpFolder+"/"+poster).toPath());
	}

	/**
	 * Get url for profile image with given id
	 * 
	 * @param id the id to get url for
	 * @return complete url used to retrieve profile image.
	 */
	public String getProfileURL(String id) {
		return downloadIfNeeded(id, profile);
	}

	/**
	 * Get the size of the files in the profile cache folder.
	 * 
	 * @return the size of the files in the profile cahce folder.
	 */
	public long getProfileCacheSize() {
		return size(new File(tmpFolder+"/"+profile).toPath());
	}

	/**
	 * Get url for still image with given id
	 * 
	 * @param id the id to get url for
	 * @return complete url used to retrieve still image.
	 */
	public String getStillURL(String id) {
		return downloadIfNeeded(id, still);
	}
	
	/**
	 * Get the size of the files in the still image cache folder.
	 * 
	 * @return the size of the files in the still image cache folder.
	 */
	public long getStillCacheSize() {
		return size(new File(tmpFolder+"/"+still).toPath());
	}

	/**
	 * Set folder to store images, used as buffer for theMovieDB.
	 * 
	 * @param tmpFolder String pointing to the tmpFolder
	 */
	public void setTmpFolder (String tmpFolder) {
		this.tmpFolder = tmpFolder;
		File f = new File (tmpFolder+"/"+backdrop);
		if (!f.exists()) {	// Sub folders for image sizes does not exists, create them
			f.mkdir();
			f = new File (tmpFolder+"/"+logo);
			f.mkdir();
			f = new File (tmpFolder+"/"+poster);
			f.mkdir();
			f = new File (tmpFolder+"/"+profile);
			f.mkdir();
			f = new File (tmpFolder+"/"+still);
			f.mkdir();
		}
	}
	
	/**
	 * Clears the disc cache, ie remove all buffered images.
	 */
	public void clearCache() {
		File f = new File (tmpFolder);
		purgeDirectoryButKeepSubDirectories(f);
	}

	/**
	 * Copied from https://stackoverflow.com/a/20043507
	 * 
	 * @param dir the base directory, remove all files beneath this directory.
	 */
	private void purgeDirectoryButKeepSubDirectories(File dir) {
		for (File file: dir.listFiles()) {
			if (file.isDirectory()) 
				purgeDirectoryButKeepSubDirectories(file);
			else
	        		file.delete();
	    }
	}
	
	/**
	 * @param id name/id of file
	 * @param size the size of the file
	 * @return URI pointing to local file system
	 */
	private String downloadIfNeeded(String id, String size) {
		String path = tmpFolder+"/"+size+"/"+id;
		File f = new File (path);
		if (!f.exists()) {
			saveImage(baseURL+size+"/"+id, path);
		} 
		return f.toURI().toString();
	}

	private void saveImage(String imageUrl, String destinationFile)  {
		URL url;
		try {
			url = new URL(imageUrl);
			InputStream is = url.openStream();
			OutputStream os = new FileOutputStream(destinationFile);

			byte[] b = new byte[2048];
			int length;

			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}

			is.close();
			os.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Attempts to calculate the size of a file or directory.@
	 * 
	 * Copied from : https://stackoverflow.com/questions/2149785/get-size-of-folder-or-file/19877372#19877372
	 * 
	 * <p>
	 * Since the operation is non-atomic, the returned value may be inaccurate.
	 * However, this method is quick and does its best.
	 */
	private long size(Path path) {

	    final AtomicLong size = new AtomicLong(0);

	    try {
	        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
	            @Override
	            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

	                size.addAndGet(attrs.size());
	                return FileVisitResult.CONTINUE;
	            }

	            @Override
	            public FileVisitResult visitFileFailed(Path file, IOException exc) {

	                System.out.println("skipped: " + file + " (" + exc + ")");
	                // Skip folders that can't be traversed
	                return FileVisitResult.CONTINUE;
	            }

	            @Override
	            public FileVisitResult postVisitDirectory(Path dir, IOException exc) {

	                if (exc != null)
	                    System.out.println("had trouble traversing: " + dir + " (" + exc + ")");
	                // Ignore errors traversing a folder
	                return FileVisitResult.CONTINUE;
	            }
	        });
	    } catch (IOException e) {
	        throw new AssertionError("walkFileTree will not throw IOException if the FileVisitor does not");
	    }

	    return size.get();
	}
}
