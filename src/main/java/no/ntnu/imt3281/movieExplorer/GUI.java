package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import no.ntnu.imt3281.movieExplorer.GUI.SearchResultItem;

public class GUI {
    @FXML private TextField searchField;
    @FXML private TreeView<SearchResultItem> searchResult;
    @FXML private Pane detailPane;
    
    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem> (new SearchResultItem(""));
    
    @FXML
    /**
     * Called when the object has been created and connected to the fxml file. All components defined in the fxml file is 
     * ready and available.
     */
    public void initialize() {
    		searchResult.setRoot(searchResultRootNode);
    		/* searchResult.getSelectionModel()			// Show media type for selected item (#oppgave 5)
            .selectedItemProperty()
            .addListener((observable, oldValue, newValue) -> System.out.println("Selected Text : " + newValue.getValue().media_type));
        */
    		searchResult.getSelectionModel()			// Add items to selected item (#oppgave 6)
            .selectedItemProperty()
            .addListener((observable, oldValue, newValue) -> {
            		if (!newValue.getValue().searched) {
	            		JSON result = null;
	            		String mediaType = null;
	            		if (newValue.getValue().media_type.equals("person")) {		// Get all movies this person is involved in
	            			result = Search.takesPartIn(newValue.getValue().id);
	            			mediaType = "movie";
		            		for (int i=0; i<result.get("results").size(); i++) {
		            			SearchResultItem item = new SearchResultItem(mediaType, result.get("results").get(i));
		            			newValue.getChildren().add(new TreeItem<SearchResultItem>(item));
		            		}
	            		} else if (newValue.getValue().media_type.equals("movie")) {	// Get everyone involved in this movie
	            			result = Search.actors(newValue.getValue().id);
	            			mediaType = "person";
		            		for (int i=0; i<result.get("cast").size(); i++) {
		            			SearchResultItem item = new SearchResultItem(mediaType, result.get("cast").get(i));
		            			newValue.getChildren().add(new TreeItem<SearchResultItem>(item));
		            		}
	            		}
	            		newValue.setExpanded(true);
	            		newValue.getValue().searched = true;
            		}
            		if (newValue.getValue().media_type.equals("movie")) {
	            		// #Oppgave7
	            		FXMLLoader loader = new FXMLLoader(getClass().getResource("movieDetails.fxml"));
	            		try {
						BorderPane pane = (BorderPane) loader.load();
						detailPane.getChildren().clear();
						detailPane.getChildren().add(pane);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

	            		MovieDetails controller = loader.<MovieDetails>getController();
	            		controller.init(newValue.getValue().id);
            		}
            });
    }

    @FXML
    /**
     * Called when the seqrch button is pressed or enter is pressed in the searchField.
     * Perform a multiSearch using theMovieDB and add the results to the searchResult tree view.
     * 
     * @param event ignored
     */
    void search(ActionEvent event) {
    		JSON result = Search.multiSearch(searchField.getText()).get("results");
    		TreeItem<SearchResultItem> searchResults = new TreeItem<> (new SearchResultItem("Searching for : "+searchField.getText()));
    		searchResultRootNode.getChildren().add(searchResults);
    		for (int i=0; i<result.size(); i++) {
    			SearchResultItem item = new SearchResultItem(result.get(i));
    			searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
    		}
    		searchResultRootNode.setExpanded(true);
    		searchResults.setExpanded(true);
    }
    
    @FXML
    /**
     * Called when the meny item "Preferences" in the "Edit" meny is selected.
     * Let the user select the folder where images downloaded from theMovieDB is
     * stored. 
     * 
     * @param event not used
     */
    void editPreferences(ActionEvent event) {
    		Preferences prefs = Preferences.userRoot().node(Class.class.getPackage().getName());
    		String tmpFolder = prefs.get("tmpFolder", "");
    		final DirectoryChooser directoryChooser = new DirectoryChooser();
    		if (!tmpFolder.equals("")) {								// If we have an active tmp folder
    			directoryChooser.setInitialDirectory(new File(tmpFolder));
    		}
    		final File selectedDirectory = directoryChooser.showDialog(MovieExplorer.stage);
    		if (selectedDirectory != null) {							// If user selected folder
    			tmpFolder = selectedDirectory.getAbsolutePath();
    			prefs.put("tmpFolder", tmpFolder);					// Store it for future use
    			MovieExplorer.config.setTmpFolder(tmpFolder);		// Set as active tmp folder
    		}
    }
    
    @FXML
    void showAbout(ActionEvent event) {
    		Dialog dialog = new Dialog();
    		dialog.setTitle("Om MovieExplorer");
    		dialog.setResizable(true);
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("aboutDialog.fxml"));
    		try {
			AnchorPane pane = (AnchorPane) loader.load();
			dialog.getDialogPane().setContent(pane);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    		AboutDialog controller = loader.<AboutDialog>getController();
    		controller.init();

    		ButtonType buttonTypeOk = new ButtonType("OK", ButtonData.OK_DONE);
    		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
    		dialog.showAndWait();
    }
    
    class SearchResultItem {
    		private String media_type = "";
    		private String name = "";
    		private long id;
    		private String profile_path = "";
    		private String title = "";
    		private boolean searched = false;
    		
    		/**
    		 * Create new SearchResultItem with the given name as what will be displayed in the tree view.
    		 * 
    		 * @param name the value that will be displayed in the tree view
    		 */
    		public SearchResultItem(String name) {
    			this.name = name;
    		}
    		
    		/**
    		 * Create a new SearchResultItem with data from this JSON object.
    		 * 
    		 * @param json contains the data that will be used to initialize this object.
    		 */
		public SearchResultItem(JSON json) {
			media_type = (String) json.getValue("media_type");
			initFromJSON(json);
		}

		/** 
		 * Create a new SearchResultItem with the given media type and data from 
		 * the given JSON object.
		 * @param mediaType the media type of this item
		 * @param json all other data about this item.
		 */
		public SearchResultItem(String mediaType, JSON json) {
			media_type = mediaType;
			initFromJSON(json);
		}

		/**
		 * Set values from JSON object, uses exisiting media type
		 * 
		 * @param json object containing information about this item.
		 */
		private void initFromJSON(JSON json) {
			if (media_type.equals("person")) {
				name = (String)json.getValue("name");	
				profile_path = (String)json.getValue("profile_path");
			} else if (media_type.equals("movie")) {
				title = (String)json.getValue("title");
			} else if (media_type.equals("tv")) {	// #oppgave 5
				name = (String)json.getValue("name");
			}
			id = (Long)json.getValue("id");
		}
    		
		/**
		 * Used by the tree view to get the value to display to the user. 
		 */
		@Override
		public String toString() {
			if (media_type.equals("person")) {
				return name;
			} else if (media_type.equals("movie")) {
				return title;
			} else if (media_type.equals("tv")) {	// #oppgave 5
				return name;
			} else {
				return name;
			}
		}
    }
}
