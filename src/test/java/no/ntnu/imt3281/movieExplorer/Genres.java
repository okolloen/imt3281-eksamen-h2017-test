package no.ntnu.imt3281.movieExplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Gets genre information from theMovieDB and stores it in the table genres in local derby database.
 * 
 * @author oeivindk
 *
 */
public class Genres {
	private static Genres genres = new Genres();
	private static HashMap<Long, String> data = new HashMap<>(); 
	
	/**
	 * Private constructor, will only be called once when the class is first read in.
	 * Gets genre information from the local database.
	 */
	private Genres() {
		try {
			Connection con = DriverManager.getConnection(MovieExplorer.dbURL);
			String sql = "SELECT id, name FROM genres";
			Statement stmnt = con.createStatement();
			ResultSet res = stmnt.executeQuery(sql);
			while (res.next()) {
				data.put(res.getLong(1), res.getString(2));
			}
		} catch (SQLException e) {
			// Unable to connect to database, somethis really bad is going on
			System.exit(1);
		}
	}

	/**
	 * Get name of genre with given id. If the id is unknown then the local database is refreshed with information from 
	 * theMovieDB.
	 * 
	 * @param id the id of the genre to get the name for
	 * @return	the name of the genre.
	 */
	public static String resolve(long id) {
		if (!data.containsKey(id)) {		// We do not have information about this genre
			refreshData();				// Update the local database (get everything fresh)
		}
		return data.get(id);				// Return the name of the genre
	}

	/**
	 * If a genre is requested and the genre id is unknown then we empty the database and gets everything fresh
	 * from theMovieDB.
	 */
	private static void refreshData() {
		try {
			Connection con = DriverManager.getConnection(MovieExplorer.dbURL);
			String sql = "DELETE FROM genres";
			Statement stmnt = con.createStatement();
			data.clear();
			stmnt.execute(sql);
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO genres (id, name) VALUES (?, ?)");
				
				// Get genres for movies
				String res = Unirest.get("https://api.themoviedb.org/3/genre/movie/list?language=en-US")
						.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
				JSON json = new JSON(res);
				for (int i=0; i<json.get("genres").size(); i++) {			// Add to database and hashmap
					ps.setLong(1, (Long)json.get("genres").get(i).getValue("id"));
					ps.setString(2, (String)json.get("genres").get(i).getValue("name"));
					data.put((Long)json.get("genres").get(i).getValue("id"), (String)json.get("genres").get(i).getValue("name"));
				}
				
				// Get genres for tv
				res = Unirest.get("https://api.themoviedb.org/3/genre/tv/list?language=en-US")
						.queryString("api_key", MovieExplorer.API_KEY).asString().getBody();
				json = new JSON(res);
				for (int i=0; i<json.get("genres").size(); i++) {			// Add to database and hashmap
					ps.setLong(1, (Long)json.get("genres").get(i).getValue("id"));
					ps.setString(2, (String)json.get("genres").get(i).getValue("name"));
					data.put((Long)json.get("genres").get(i).getValue("id"), (String)json.get("genres").get(i).getValue("name"));
				}				
			} catch (UnirestException e) {
				// Well, this is highly unlikely.
			}
		} catch (SQLException e) {
			// Unable to connect to database, somethis really bad is going on
			System.exit(1);
		}
	}
}
